import React, {Component} from 'react';
import ReactDom from 'react-dom';
import main from 'components/blocks/main';



ReactDom.render(
    <div className="container-fluid">
        <div className="row">
            <div className="col-12">
                <main />
            </div>
        </div>
    </div>,
    document.getElementById("root")
);


